#!/usr/bin/env python
"""hyperturnips installation script."""

import setuptools

def main():
    """hyperturnips installation wrapper"""
    kwargs = {
        'name': 'hyperturnips',
        'use_scm_version': True,
        'author': 'nago',
        'author_email': 'nago@malie.io',
        'description': 'turnips are vegetables with a creamy white color and a purple top.',
        'url': 'https://gitlab.com/nanoNago/hyperturnips',
        'packages': setuptools.find_packages(),
        'classifiers': [
            "Development Status :: 2 - Pre-Alpha",
            "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
            "Natural Language :: English",
            "Operating System :: OS Independent",
            "Programming Language :: Python :: 3",
        ],
        'setup_requires': [
            'setuptools_scm',
        ],
        'install_requires': [
            'setuptools_scm',
            'flask',
            'flask-sqlalchemy',
            'flask-security-too',
            'bcrypt',
            'uwsgi',
            'turnips >= 0.6',
        ],
        'extras_require': {
            'develop': [
                'flask-debugtoolbar',
            ],
        },
        'python_requires': '>=3.7',
	'entry_points': {
            'console_scripts': [
            ]
        },
    }

    with open("README.rst", "r") as fh:
        kwargs['long_description'] = fh.read()

    setuptools.setup(**kwargs)

if __name__ == '__main__':
    main()
