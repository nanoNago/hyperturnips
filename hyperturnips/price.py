import datetime
import hashlib
import itertools
import traceback
from typing import Dict, Optional, Tuple, Union
import os

from flask import (
    Blueprint,
    current_app,
    flash,
    g,
    redirect,
    render_template,
    request,
    url_for,
    send_from_directory,
)
from werkzeug.exceptions import abort
from flask_security import login_required

import turnips
from turnips.archipelago import IslandModel, Island
from turnips.model import ModelEnum
from turnips.ttime import TimePeriod

from hyperturnips.models import User, TurnipPrice, SQLIsland, PatternHash
from hyperturnips.db import DB
import hyperturnips

bp = Blueprint("price", __name__)


class Week:
    def __init__(self,
                 date_or_weekid: Optional[Union[
                     datetime.date,
                     Tuple[Optional[int], Optional[int]]
                 ]]):
        """
        Create a Week object from any of the following:
        date: Any date, Sunday or not.
        (year, week_number)
        nothing: use server's Today date and get that week.
        """
        self._sunday: datetime.date
        self._week: Dict[str, datetime.date] = {}

        if isinstance(date_or_weekid, tuple):
            self.ac_week_id = date_or_weekid
            return

        date = date_or_weekid
        self.set_week_of(date)

    def set_week_of(self, date: Optional[datetime.date] = None) -> None:
        date = self.get_date(date)
        self.sunday = self.find_sunday(date)

    @property
    def sunday(self):
        return self._sunday

    @sunday.setter
    def sunday(self, date: datetime.date):
        if date.isoweekday() != 7:
            raise ValueError(f"Date {str(date)} is not a Sunday")
        self._sunday = date
        self._generate_week()

    @property
    def year(self):
        """
        Returns the year this week is in.

        NOTE: If this week straddles a year, the year will be the calendar
        year that this week's Sunday occurred within.
        """
        return self._sunday.year

    @property
    def week_number(self):
        return int(self._sunday.strftime('%U'))

    @property
    def ac_week_id(self):
        return (self.year, self.week_number)

    @ac_week_id.setter
    def ac_week_id(self, value: Tuple[Optional[int], Optional[int]]):
        year, week_number = value
        if year is None and week_number is None:
            self.set_week_of()
            return

        if year is None:
            # Assume this year, I guess?
            year = self.get_date().year

        # I don't know what this should even mean, so prohibit it for now
        assert week_number is not None, "Week number must be specified if year was provided"

        spec_week = f"{year:04d}:Sunday:{week_number:02d}"
        sunday = datetime.datetime.strptime(spec_week, '%Y:%A:%U').date()
        self.sunday = sunday

    def _generate_week(self) -> None:
        for i in range(0, 7):
            day = self.sunday + datetime.timedelta(days=i)
            self._week[day.strftime('%A')] = day

    @property
    def next_week(self) -> datetime.date:
        next_sunday = self.sunday + datetime.timedelta(days=7)
        assert next_sunday.isoweekday() == 7, "BUG: next_sunday didn't return a Sunday"
        return type(self)(next_sunday)

    @property
    def last_week(self) -> datetime.date:
        last_sunday = self.sunday - datetime.timedelta(days=7)
        assert last_sunday.isoweekday() == 7, "BUG: last_sunday didn't return a Sunday"
        return type(self)(last_sunday)

    # Mapping

    def __getitem__(self, key: str):
        return self._week[key]

    def __iter__(self):
        return iter(self._week)

    def __len__(self):
        return len(self._week)

    def keys(self):
        return self._week.keys()

    def items(self):
        return self._week.items()

    def values(self):
        return self._week.values()

    def __contains__(self, key: str):
        return key in self._week

    @classmethod
    def get_date(cls, today: Optional[datetime.date] = None) -> datetime.date:
        if today is None:
            return datetime.date.today()
        return today

    @classmethod
    def find_sunday(cls, date: datetime.date):
        this_week = date.strftime('%Y:Sunday:%U')
        sunday = datetime.datetime.strptime(this_week, '%Y:%A:%U').date()
        return sunday


# ---- #


def get_user_week(user_id: int,
                  week: Optional[Week] = None) \
                  -> Dict[TimePeriod, Optional[TurnipPrice]]:
    if week is None:
        week = Week()
    prices = TurnipPrice.query.filter(
        TurnipPrice.date >= week['Sunday'],
        TurnipPrice.date <= week['Saturday'],
        TurnipPrice.uid == user_id
    ).order_by(
        TurnipPrice.date.asc(),
        TurnipPrice.afternoon.asc()
    ).all()

    by_date = {}
    for price in prices:
        meridian = "PM" if price.afternoon else "AM"
        by_date.setdefault(price.date, {})[meridian] = price

    by_period = {}
    for weekday, afternoon in itertools.product(week, (False, True)):
        meridian = "PM" if afternoon else "AM"
        name = f"{weekday}_{meridian}"
        tpe = TimePeriod.normalize(name)
        price = by_date.get(week[weekday], {}).get(meridian)
        by_period[tpe] = price

    return {
        'by_date': by_date,
        'by_period': by_period,
    }


def get_user_island_model(user_id: int, week: Week) -> \
    Tuple[
        IslandModel,
        Dict[TimePeriod, Optional[TurnipPrice]]
    ]:
    db_timeline = get_user_week(user_id, week)
    model_data = {
        'previous_week': ModelEnum.unknown,
        'initial_week': False,
        'timeline': {key: tpe.price for key, tpe in db_timeline['by_period'].items() if tpe is not None}
    }
    model = IslandModel.parse_obj(model_data)
    return model, db_timeline


def get_last_week_pattern(user_id: int, week: Week):
    previous_model, _ = get_user_island_model(user_id, week.last_week)

    last_prices = f"{str(previous_model.timeline)}".encode()
    last_prices_hash = hashlib.md5(last_prices).hexdigest()

    cached = PatternHash.query.filter(
        PatternHash.md5 == last_prices_hash
    ).first()

    if cached is None:
        previous = Island("", previous_model)
        patterns_set = set(model.model_name for model in previous.model_group.models)
        patterns = sorted(patterns_set)
        if patterns:
            cached = PatternHash(md5=last_prices_hash, patterns=",".join(patterns))
            DB.session.add(cached)
    else:
        patterns = sorted(cached.patterns.split(","))

    if len(patterns) == 1 and patterns[0]:
        try:
            return ModelEnum[patterns[0]]
        except KeyError:
            pass

    return ModelEnum.unknown


def get_user_island(user_id: int,
                    week: Optional[Week] = None):
    if week is None:
        week = Week()

    model, db_timeline = get_user_island_model(user_id, week)

    db_island = SQLIsland.query.filter(
        SQLIsland.uid == user_id
    ).first()

    if db_island:
        island_name = db_island.name
        first_week = db_island.first_week
    else:
        island_name = ""
        first_week = None

    model.initial_week = first_week == week.sunday
    model.previous_week = get_last_week_pattern(user_id, week)
    return Island(island_name, model), db_timeline, db_island


# ---- #

def get_active_weekly_users(week: Optional[Week] = None):
    if week is None:
        week = Week()

    # Get users, sorted by most-recently-updated prices
    users = User.query.join(TurnipPrice).filter(
        TurnipPrice.date >= week['Sunday'],
        TurnipPrice.date <= week['Saturday'],
        TurnipPrice.price.isnot(None)
    ).order_by(
        TurnipPrice.date.desc(),
        TurnipPrice.afternoon.desc()
    ).all()

    return users


def render_island_graph(uisland):
    checksum = uisland.checksum()
    imgname = f"{checksum}.png"
    folder = os.path.join(current_app.instance_path, 'plots')
    path = os.path.join(folder, imgname)
    if not os.path.exists(path):
        os.makedirs(folder, exist_ok=True)
        plotdata = uisland.plot()
        with open(path, "wb") as outfile:
            outfile.write(plotdata)
    return imgname


@bp.route("/")
@bp.route("/view/", defaults={
    'year': None,
    'week_number': None,
})
@bp.route('/view/<int:year>/<int:week_number>/')
def view(year: Optional[int] = None, week_number: Optional[int] = None):
    """
    Show the prices and graphs for active users this week.
    """

    islands = []
    week = Week((year, week_number))
    active_users = get_active_weekly_users(week)

    errors = []
    for user in active_users:
        try:
            uisland, db_timeline, _db_island = get_user_island(user.id, week)
        except:
            current_app.logger.error(traceback.format_exc())
            errors.append(f"Can't resolve {user.username}'s island-week;"
                          " please get nago's attention on discord")
            continue

        try:
            imgname = render_island_graph(uisland)
        except:
            current_app.logger.error(traceback.format_exc())
            errors.append(f"Can't graph {user.username}'s island-week;"
                          " please get nago's attention on discord")
            continue

        islands.append({
            'user': user,
            'island': uisland,
            'timeline': db_timeline,
            'img': imgname,
        })

    for error in errors:
        flash(error, 'error')
    else:
        # Cache data, maybe
        DB.session.commit()

    data = {
        'week': week,
        'islands': islands,
        'update_url': url_for('price.update', year=year, week_number=week_number),
    }

    return render_template("price/view.html",
                           data=data)


@bp.route('/img/<string:filename>')
def img(filename: str):
    try:
        folder = os.path.join(current_app.instance_path, 'plots')
        return send_from_directory(folder, filename)
    except FileNotFoundError:
        abort(404, "FileNotFound")


def get_time_period_label(day, meridian):
    if day == 'Sunday':
        return 'Buy Price'
    return f"{day} ({meridian})"


def get_time_period_bounds(day, meridian):
    if day == 'Sunday':
        return 90, 110
    return 9, 660


@bp.route('/update/', methods=('GET', 'POST'), defaults={
    'year': None,
    'week_number': None,
})
@bp.route('/update/<int:year>/<int:week_number>/', methods=('GET', 'POST'))
@login_required
def update(year: int = None, week_number: int = None):

    errors = []
    fields = {}
    week = Week((year, week_number))
    ask_first_week = True
    uisland, timeline, db_island = get_user_island(g.user.id, week)

    def _parse_price(field, price_str):
        if not price_str:
            return None
        try:
            price = int(price_str)
        except ValueError:
            errors.append(f"Invalid Price: {price_str}")
            raise
        if not field['min'] <= price <= field['max']:
            errors.append(f"Price {price} is out of range [9, 660]")
            raise ValueError
        return price

    # If the user set a first_week, but it's not this week, hide this checkbox
    if db_island.first_week is not None and not uisland.initial_week:
        ask_first_week = False

    for day, date in week.items():
        for afternoon in (False, True):
            if day == 'Sunday' and afternoon:
                continue

            meridian = 'PM' if afternoon else 'AM'
            low, high = get_time_period_bounds(day, meridian)

            fields[(date, afternoon)] = {
                'day': day,
                'id': f"{day}_{meridian}".lower(),
                'label': get_time_period_label(day, meridian),
                'tprice': timeline['by_date'].get(date, {}).get(meridian, None),
                'min': low,
                'max': high,
            }

    if request.method == "POST":
        if request.form.get('firstweek') == 'on':
            db_island.first_week = week.sunday
        elif ask_first_week:
            db_island.first_week = None

        prices = list(request.form.getlist('prices[]'))
        assert len(prices) == 13
        for (date, afternoon), field in fields.items():
            try:
                price = _parse_price(field, prices.pop(0))
            except ValueError:
                continue

            tp = field['tprice']
            if tp is None:
                tp = TurnipPrice(uid=g.user.id,
                                 date=date,
                                 afternoon=afternoon,
                                 price=price)
                DB.session.add(tp)
            tp.price = price

        if not errors:
            DB.session.commit()
            flash("Prices saved!", 'success')
            return redirect(url_for('price.view', year=year, week_number=week_number))

    for error in errors:
        flash(error, 'error')

    return render_template(
        "price/update.html",
        fields=fields.values(),
        week=week,
        ask_first_week=ask_first_week,
        is_first_week=uisland.initial_week,
    )


def _set_globals():
    g.turnips = turnips.get_version()
    g.hyperturnips = hyperturnips.get_version()


def init_app(app):
    app.before_request(_set_globals)

