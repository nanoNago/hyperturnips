import os
import pkg_resources
import setuptools_scm

from flask import Flask
try:
    TOOLBAR = True
    from flask_debugtoolbar import DebugToolbarExtension
except ModuleNotFoundError:
    TOOLBAR = False

from hyperturnips import (
    price,
    island,
    security,
)

if TOOLBAR:
    _toolbar = DebugToolbarExtension()

PKGNAME = 'hyperturnips'


def install_version() -> str:
    return pkg_resources.get_distribution(PKGNAME).version


def live_version() -> str:
    version = setuptools_scm.get_version(root='..', relative_to=__file__)
    return version


def get_version() -> str:
    try:
        version = live_version()
    except LookupError:
        version = install_version()
    return version


def create_app(test_config=None):
    """
    Create and configure an instance of the Flask application.
    """
    app = Flask(__name__, instance_relative_config=True)

    # store the database in the instance folder
    path = os.path.join(app.instance_path, 'hyperturnips.sqlite')
    uri = "sqlite:///{}".format(path)

    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY="dev",
        SECURITY_PASSWORD_SALT='super-secret-random-salt',

        SQLALCHEMY_DATABASE_URI=uri,
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SECURITY_REGISTERABLE=True,
        SECURITY_SEND_REGISTER_EMAIL=False,
        SECURITY_USER_IDENTITY_ATTRIBUTES=['username', 'email'],
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        app.config.update(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Register security (and db) commands;
    # then register the price module (which takes the 'index' endpoint.)
    security.init_app(app)
    app.register_blueprint(price.bp)
    app.register_blueprint(island.bp)
    price.init_app(app)

    app.add_url_rule("/", endpoint="index")
    if TOOLBAR:
        _toolbar.init_app(app)

    return app
