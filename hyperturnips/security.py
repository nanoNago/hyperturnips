from flask import g
from flask_security import (
    LoginForm,
    RegisterForm,
    SQLAlchemyUserDatastore,
    Security,
)
from flask_security.forms import (
    email_required,
    Required,
    StringField,
)
from flask_security.core import current_user

from hyperturnips.models import User, Role
from hyperturnips import db
from hyperturnips.db import DB

security = Security()
user_datastore = SQLAlchemyUserDatastore(DB, User, Role)

class FlaskrLoginForm(LoginForm):
    # Note: clumsily overrides the default label for Email,
    # because I can't resolve the lazystring from get_form_field_label at instantiation time
    email = StringField("Username or Email", validators=[email_required])

class FlaskrRegisterForm(RegisterForm):
    username = StringField('Username', [Required()])


def load_logged_in_user():
    """
    If the user is logged in, cache their user object to g.user.
    """
    if current_user.is_authenticated:
        g.user = current_user
    else:
        g.user = None


def initialize_tables():
    """Create any tables needed, if any."""
    DB.create_all()


def init_app(app):
    """Initialize SQLAlchemy and flask-security."""
    db.init_app(app)
    security.init_app(app, datastore=user_datastore,
                      login_form=FlaskrLoginForm,
                      register_form=FlaskrRegisterForm)

    # Initialize the tables on-load.
    with app.app_context():
        initialize_tables()

    # Register the on-request callback.
    app.before_request(load_logged_in_user)
