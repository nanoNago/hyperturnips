import datetime

from flask_security import (
    UserMixin,
    RoleMixin,
)

from hyperturnips.db import DB

roles_users = (
    DB.Table('roles_users',
             DB.Column('user_id', DB.Integer(), DB.ForeignKey('user.id')),
             DB.Column('role_id', DB.Integer(), DB.ForeignKey('role.id'))))


class Role(DB.Model, RoleMixin):
    id = DB.Column(DB.Integer(), primary_key=True)
    name = DB.Column(DB.String(80), unique=True)
    description = DB.Column(DB.String(255))


class Membership(DB.Model):
    __tablename__ = 'membership'
    aid = DB.Column(
        DB.Integer,
        DB.ForeignKey('archipelagos.aid'),
        primary_key=True
    )
    uid = DB.Column(
        DB.Integer,
        DB.ForeignKey('user.id'),
        primary_key=True
    )


class User(DB.Model, UserMixin):
    id = DB.Column(DB.Integer, primary_key=True)
    username = DB.Column(DB.String(255), unique=True)
    email = DB.Column(DB.String(255), unique=True)
    password = DB.Column(DB.String(255))
    active = DB.Column(DB.Boolean())
    confirmed_at = DB.Column(DB.DateTime())
    #
    friendcode = DB.Column(DB.String)
    prices = DB.relationship('TurnipPrice', back_populates="user")
    roles = DB.relationship('Role', secondary=roles_users,
                            backref=DB.backref('users', lazy='dynamic'))
    archipelagos = DB.relationship(
        'Archipelago',
        secondary=Membership.__table__,
        back_populates='members'
    )


class SQLIsland(DB.Model):
    __tablename__ = 'islands'
    iid = DB.Column(DB.Integer, primary_key=True)
    uid = DB.Column(DB.Integer, DB.ForeignKey('user.id'))
    name = DB.Column(DB.String)
    fruit = DB.Column(DB.String)
    southern_hemisphere = DB.Column(DB.Boolean)
    first_week = DB.Column(DB.Date)


class TurnipPrice(DB.Model):
    __tablename__ = 'turnip_prices'
    uid = DB.Column(DB.Integer, DB.ForeignKey('user.id'), primary_key=True)
    date = DB.Column(DB.Date, primary_key=True)
    afternoon = DB.Column(DB.Boolean, primary_key=True)
    price = DB.Column(DB.Integer)
    #
    user = DB.relationship('User', back_populates="prices")

    @classmethod
    def get(cls, uid, date, afternoon):
        return cls.query.filter(
            cls.uid == uid,
            cls.date == date,
            cls.afternoon == afternoon
        ).first()


class Archipelago(DB.Model):
    __tablename__ = 'archipelagos'
    aid = DB.Column(DB.Integer, primary_key=True)
    uid = DB.Column(DB.Integer, DB.ForeignKey('user.id'))
    name = DB.Column(DB.String, unique=True)
    #
    captain = DB.relationship('User')
    members = DB.relationship(
        'User',
        secondary=Membership.__table__,
        back_populates='archipelagos',
    )


class PatternHash(DB.Model):
    __tablename__ = 'pattern_hashes'
    md5 = DB.Column(DB.String, primary_key=True)
    patterns = DB.Column(DB.String)
