from enum import Enum

from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    request,
    url_for,
)
from werkzeug.exceptions import abort
from flask_security import login_required

from hyperturnips.models import SQLIsland
from hyperturnips.db import DB

bp = Blueprint("island", __name__)

class FruitStr(Enum):
    apple = "Apple"
    cherry = "Cherry"
    orange = "Orange"
    peach = "Peach"
    pear = "Pear"

@bp.route("/island", methods=("GET", "POST"))
@login_required
def island():
    island = SQLIsland.query.filter(SQLIsland.uid == g.user.id).first()

    if island is None:
        island = SQLIsland(uid=g.user.id)
        DB.session.add(island)

    if request.method == "POST":
        errors = []
        name = request.form['island_name']
        fruit = request.form['fruit']
        hemisphere = request.form['hemisphere']

        if not name:
            errors.append("Island Name is required.")

        if fruit:
            try:
                fruit = FruitStr[fruit]
            except KeyError:
                errors.append(f"Invalid Fruit: {fruit}")

        if hemisphere and hemisphere not in ('north', 'south'):
            errors.append(f"I'm not too familiar with the {hemisphere} hemisphere...")

        if errors:
            for error in errors:
                flash(error, 'error')
            return render_template("island/index.html", island=island, frenum=FruitStr)

        island.name = name
        if fruit:
            island.fruit = fruit.value
        if hemisphere == 'north':
            island.southern_hemisphere = False
        else:
            island.southern_hemisphere = True

    if DB.session.is_modified(island):
        DB.session.commit()
        flash("Saved Successfully!", 'success')

    return render_template("island/index.html", island=island, frenum=FruitStr)
