Hyperturnips
============

Hyperturnips is a small web utility to allow you and your friends to
share turnip prices.

Setup
-----

Requires the ``turnips`` library, which is not-yet represented as a
pip dependency, and will have to be installed manually.

Otherwise, the usual flask tricks will get you into development mode:

- ``FLASK_ENV=development``
- ``FLASK_APP=hyperturnips``
- ``flask init-db``
- ``flask run``
